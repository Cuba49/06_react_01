import React from "react";
import "./style.css"
import Header from "../Header";
import MessageInput from "../MessageInput";
import MessageList from "../MessageList";
import Preloader from "../Preloader";

class Chat extends React.Component {
    constructor() {
        super();
        this.state = {
            messages: [],
            myId: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
            editMessageId: null,
            loadMesseges: true
        }
    }
    componentDidMount() {
        const url = this.props.url;
        fetch(url).then(responce => {
            return responce.json();
        }
        ).then(responce => {
            this.setState({ messages: responce.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt)), loadMesseges: false });
        })

    }
    getCountUsers() {
        const uniqueUsers = [];
        this.state.messages.forEach(message => {
            const userId = message.userId;
            if (uniqueUsers.indexOf(userId) === -1) {
                uniqueUsers.push(userId);
            }
        })
        return uniqueUsers.length;
    }
    getLastMessageAt() {
        const sortMessages = this.state.messages;
        return sortMessages.length > 0 ? sortMessages[0].createdAt : null;
    }
    onNewMessage(text) {
        if (this.state.editMessageId !== null) {
            const newMessages = this.state.messages.map(message => {
                if (message.id === this.state.editMessageId) {
                    message.text = text;
                    message.editedAt = new Date().toISOString();
                }
                return message;
            });
            this.setState({ messages: newMessages, editMessageId: null });

        } else {
            const newMessage = { createdAt: new Date().toISOString(), editedAt: "", avatar: "", id: Date.now(), text: text, user: "Me", userId: this.state.myId };
            this.state.messages.unshift(newMessage);
            this.setState({ messages: this.state.messages });
        }
    }
    onDelete(id) {
        this.setState({ messages: this.state.messages.filter(message => message.id !== id) });
    }
    onEdit(id) {
        this.setState({ editMessageId: id });
    }
    render() {
        const textToInput = this.state.editMessageId !== null ? this.state.messages.filter(message => message.id === this.state.editMessageId)[0]?.text : '';
        const isLoad = this.state.loadMesseges === true ? <Preloader /> : [];
        return (
            <div className="chat">
                <Header
                    name="Вселенский заговор"
                    countUsers={this.getCountUsers()}
                    countMessages={this.state.messages.length}
                    lastMessageAt={this.getLastMessageAt()}
                />
                <MessageList
                    onDelete={(id) => this.onDelete(id)}
                    onEdit={(id) => this.onEdit(id)}
                    messages={this.state.messages}
                    myId={this.state.myId} />
                <MessageInput text={textToInput} onSend={(text) => this.onNewMessage(text)} />
                {isLoad}
            </div>
        );
    }
}
export default Chat;