import React from "react";
import "./style.css";
class MessagesDivider extends React.Component {
    render() {
        return (
            <div className="messages-divider">
                <div className="messages-divider-date">{this.props.date}</div>
                <div className="messages-divider-line"></div>
            </div>
        );
    }
}
export default MessagesDivider;
