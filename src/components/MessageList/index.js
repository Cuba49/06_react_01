import React from "react";
import "./style.css"
import Message from "../Message";
import OwnMessage from "../OwnMessage";
import MessagesDivider from "../MessagesDivider";
class MessageList extends React.Component {

    getViewDate(dateIso) {
        const days = ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        const date = new Date(dateIso);
        const today = new Date();
        if (date.getFullYear() === today.getFullYear() && date.getMonth() === today.getMonth() && date.getDate() === today.getDate()) {
            return "Today";
        } else if (date.getTime() > new Date("" + (today.getMonth() + 1) + "." + today.getDate() + "." + today.getFullYear()).getTime() - 1000 * 60 * 60 * 24) {
            return "Yesterday";
        } else {
            const day = days[date.getDay()];
            const number = date.getDate();
            let month = months[date.getMonth()];
            return day + ", " + number + " " + month;
        }

    }
    render() {
        const myId = this.props.myId;
        const messagesBlocks = [];
        let contextDate = null;
        this.props.messages.forEach(message => {
            if (this.getViewDate(message.createdAt) !== contextDate && contextDate !== null) {
                messagesBlocks.unshift(<MessagesDivider date={contextDate} key={message.createdAt} />);
            }
            contextDate = this.getViewDate(message.createdAt);
            messagesBlocks.unshift(
                message.userId === myId ?
                    <OwnMessage key={message.id} message={message} onDelete={() => this.props.onDelete(message.id)} onEdit={() => this.props.onEdit(message.id)} />
                    : <Message key={message.id} message={message} />
            )
        });

        if (contextDate !== null) {
            messagesBlocks.unshift(<MessagesDivider date={contextDate} key={contextDate} />);
        }
        return (<div className="message-list" id="message-list">
            <div className="message-list-scroll">{messagesBlocks}</div>
        </div>);
    }
}
export default MessageList;