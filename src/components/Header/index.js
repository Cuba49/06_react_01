import React from "react";
import "./style.css"

class Header extends React.Component {
    convertDate() {
        const date = new Date(this.props.lastMessageAt);
        let hours = date.getHours();
        hours = hours < 10 ? "0" + hours : hours;
        let minutes = date.getMinutes();
        minutes = minutes < 10 ? "0" + minutes : minutes;
        const month = date.getMonth() + 1;
        const day = date.getDate();
        return (day < 10 ? "0" + day : day) + "." + (month < 10 ? "0" + month : month) + "." + date.getFullYear()+" "+hours + ":" + minutes;
    }
    render() {
        return (
            <div className="header">
                <div className="header-left">
                    <p className="header-title">{this.props.name}</p>
                    <p className="grey-text header-users-count">{this.props.countUsers}</p><p className="grey-text">participants</p>
                    <p className="grey-text header-messages-count">{this.props.countMessages}</p><p className="grey-text">messages</p>
                </div>
                <div className="header-right grey-text">
                    <p>last message </p>
                    <p className="header-last-message-date">{this.convertDate()}</p>
                </div>
            </div>
        );
    }
}
export default Header;