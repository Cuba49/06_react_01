import React from "react";
import "./style.css"
class OwnMessage extends React.Component {
    getTime() {
        const date = new Date(this.props.message.createdAt)
        let hours = date.getHours();
        hours = hours < 10 ? "0" + hours : hours;
        let minutes = date.getMinutes();
        minutes = minutes < 10 ? "0" + minutes : minutes;
        return hours + ":" + minutes;
    }
    render() {
        const message = this.props.message;
        return (<div className="own-message">
            <div className="message-body message-body-my">
                <p className="message-text">{message.text}</p>
                <div className="message-time">{this.getTime()}</div>
            </div>
            <i className="fas fa-edit message-edit" onClick={() => this.props.onEdit()}></i>
            <i className="far fa-trash-alt message-delete" onClick={() => this.props.onDelete()}></i>
        </div>);
    }
}
export default OwnMessage;