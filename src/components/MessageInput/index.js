import React from "react";
import "./style.css"
class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.startTime !== this.state.text) {
            this.setState({ text: nextProps.text });
        }
    }
    onChange(e) {
        const value = e.target.value;
        this.setState({ text: value })
    }
    render() {
        return (<div className="message-input">
            <textarea className="message-input-text" type="text" placeholder="Type your message" onChange={(e) => this.onChange(e)} value={this.state.text} />
            <button className="message-input-button" onClick={() => { this.props.onSend(this.state.text); this.setState({ text: '' }) }}>SEND</button>
        </div>);
    }
}
export default MessageInput;