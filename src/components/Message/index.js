import React from "react";
import "./style.css"
class Message extends React.Component {
    constructor() {
        super();
        this.state = {
            liked: false
        }
    }
    like() {
        this.setState({ liked: !this.state.liked })
    }
    getTime() {
        const date = new Date(this.props.message.createdAt)
        let hours = date.getHours();
        hours = hours < 10 ? "0" + hours : hours;
        let minutes = date.getMinutes();
        minutes = minutes < 10 ? "0" + minutes : minutes;
        return hours + ":" + minutes;
    }
    render() {
        const message = this.props.message;
        const likeClass = this.state.liked ? "fas fa-heart message-liked" : "far fa-heart message-like";
        return (<div className="message">
            <img src={message.avatar} alt={message.user} className="message-user-avatar" />
            <div className="message-body">
                <div>
                    <p className="message-user-name">{message.user}</p>
                    <p className="message-text">{message.text}</p>
                </div>
                <div className="message-time">{this.getTime()}</div>
                <i className={likeClass} onClick={() => { this.like() }}></i>
            </div>
        </div>);
    }
}
export default Message;