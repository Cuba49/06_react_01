import React from "react";
import "./style.css"
class Preloader extends React.Component {
    render() {
        return (<div className="preloader">
            <div className="loader"></div>
        </div>);
    }
}
export default Preloader;